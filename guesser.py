def guess(word_list,contains,right_position,wrong_letters):
    possible_words=[] #list of possible words
    for word in word_list:
        possible=True
        wrong_letter=False #every word is possible until proven otherwise

        for i in range(len(right_position)):#checking the green letters
            if right_position[i]=="0":continue #0 means that i haven't found the correct letter in this position
            if(word[i]!=right_position[i]):
                possible=False #if word has a different letter than what i've found in position i, then this word is not the correct one
                break
        
        if(not possible):continue #continue to the next word 

        for letter in wrong_letters: #searching for the "black" letters
            if letter in word:
                wrong_letter=True #if i've found the word with black letter, then this isnt a correct word
        
        if wrong_letter:continue #continue to the next word 
        

        for letter in contains: #checking the yellow letters
            if letter not in word: #if this "word" doesnt have the "yellow" letter, then its not a correct word
                possible=False
                break
            
        if(possible):possible_words.append(word)#if the "word" is a possible solution, then add it to my list
    return possible_words


def main():
    words=open("words.txt","r")
    word_list=words.read().splitlines()
    while(True):
        right_position=input("give string with the letters in the correct position you found, add 0 to the positions you didn't find (ex:0e000 for e in 2nd position),or hit enter if you didn't find any:")
        contains=input("give the letters that the word contains, or hit enter if you didn't find any:")
        wrong_letters=input("give the letters that the word doesn't contain, or hit enter if you didn't find any:")
        print()
        print(guess(word_list,contains,right_position,wrong_letters))
        print()
        x=input("shall we continue? press y or n:")
        if(x=='n'):break
    



if __name__=="__main__":
    main()


