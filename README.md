# Wordle_Guesser

Python script used to guess the correct wordle

## Instructions

1) input the letters you've found in the correct position (green), use 0 for the position you haven't found a letter (example: br00k for break)
2) input the letters you've found in the wrong position (yellow) one next to another (example: ae for break)
3) input the wrong letters you've found one next to another (example: sl for break)


# TO_DO
1) add method for duplicate letters
2) add method to exclude "yellow" letters in the wrong position

